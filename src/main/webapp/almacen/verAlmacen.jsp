<%@include file="../admin/cabecera.jsp" %>
<table class="table table-dark table-sm">
    <thead>
        <tr>
            <th scope="col">Cantidad</th>
            <th scope="col">Precio almacenado</th>
            <th scope="col">Acciones</th>

        </tr>
    </thead>
    <tbody>           
        <c:forEach var="al" items="${almacenes}">
            <tr>
                <td>${al.cantidadProductos}</td>
                <td>${al.precioAlmacenado}</td>
                <td>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/almacen?id=123456&accion=editar">Editar</a>
                </td>
            </tr>                  
        </c:forEach>
    </tbody>
</table>
<%@include file="../admin/pie.jsp" %>


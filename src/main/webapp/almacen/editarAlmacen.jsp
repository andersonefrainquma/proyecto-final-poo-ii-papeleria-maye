<%@include file="../admin/cabecera.jsp" %>
<form method="post" action="${pageContext.request.contextPath}/almacen?accion=modificar&id=123456">

    <label for="validationCustom01" class="form-label">ID:</label>
    <input type="number" class="form-control is-valid" value="123456" disabled="disable" id="id" name="id" required>

    <label for="validationCustom01" class="form-label">Digite la cantidad de productos:</label>
    <input type="number" class="form-control is-valid" value="${almacen.cantidadProductos}" id="cantidadP" name="cantidadP" required>

    <label for="validationCustom01" class="form-label">Precio almacenado:</label>
    <input type="number" class="form-control is-valid" value="${almacen.precioAlmacenado}" id="precioAlmacenado" name="precioAlmacenado" required>

    <button class="btn btn-primary" type="submit">Guardar</button>
</form>
<%@include file="../admin/pie.jsp" %>

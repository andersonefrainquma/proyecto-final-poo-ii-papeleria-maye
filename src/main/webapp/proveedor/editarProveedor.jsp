<%@include file="../admin/cabecera.jsp" %>
<form method="post" action="${pageContext.request.contextPath}/proveedor?accion=modificar&cedula=${proveedor.cedula}">

    <label for="validationCustom01" class="form-label">Digite la c�dula:</label>
    <input type="number" class="form-control is-valid" value="${proveedor.cedula}" disabled="disable" id="cedula" name="cedula" required>

    <label for="validationCustom01" class="form-label">Digite el Nombre:</label>
    <input type="text" class="form-control is-valid" value="${proveedor.nombre}" id="nombre" name="nombre" required>

    <button class="btn btn-primary" type="submit">Guardar</button>
</form>
<%@include file="../admin/pie.jsp" %>

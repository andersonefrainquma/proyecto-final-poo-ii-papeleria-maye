<%@include file="../admin/cabecera.jsp" %>
<a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/proveedor/agregarProveedor.jsp">Agregar proveedor</a>
<table class="table table-dark table-sm">
    <thead>
        <tr>
            <th scope="col">Cedula</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>

        </tr>
    </thead>
    <tbody>           
        <c:forEach var="pro" items="${proveedores}">
            <tr>
                <td>${pro.cedula}</td>
                <td>${pro.nombre}</td>
                <td>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/proveedor?cedula=${pro.cedula}&accion=borrar">Eliminar</a>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/proveedor?cedula=${pro.cedula}&accion=editar">Editar</a>
                </td>
            </tr>                  
        </c:forEach>
    </tbody>
</table>
<%@include file="../admin/pie.jsp" %>
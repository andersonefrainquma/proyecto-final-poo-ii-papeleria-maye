
<%@include file="../admin/cabecera.jsp" %>
        <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/factura/agregarFactura.jsp">Agregar factura</a>
        <table class="table table-dark table-sm">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Acciones</th>

                </tr>
            </thead>
            <tbody>           
                <c:forEach var="fac" items="${facturas}">
                    <tr>
                        <td>${fac.id_factura}</td>
                        <td>${fac.precioTotal}</td>
                        <td>
                            <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/factura?id_factura=${fac.id_factura}&accion=borrar">Eliminar</a>
                            <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/factura?id_factura=${fac.id_factura}&accion=editar">Editar</a>
                        </td>
                    </tr>                  
                </c:forEach>
            </tbody>
        </table>
<%@include file="../admin/pie.jsp" %>

<%@include file="../admin/cabecera.jsp" %>
<table class="table table-dark table-sm">
    <thead>
        <tr>
            <th scope="col">dinero recibido</th>
            <th scope="col">vueltos</th>
            <th scope="col">contador de ventas</th>
            <th scope="col">dinero acumulado</th>
            <th scope="col">Acciones</th>

        </tr>
    </thead>
    <tbody>           
        <c:forEach var="c" items="${cajas}">
            <tr>
                <td>${c.dineroRecibido}</td>
                <td>${c.vueltos}</td>
                <td>${c.contadorVentas}</td>
                <td>${c.dineroAcumulado}</td>   
                <td>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/caja?id=123456&accion=editar">Editar</a>
                </td>
            </tr>                  
        </c:forEach>
    </tbody>
</table>
<%@include file="../admin/pie.jsp" %>

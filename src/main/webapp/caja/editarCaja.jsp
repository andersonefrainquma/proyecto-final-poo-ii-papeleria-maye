<%@include file="../admin/cabecera.jsp" %>
        <form method="post" action="${pageContext.request.contextPath}/caja?accion=modificar&id=123456">
           
            <label for="validationCustom01" class="form-label">ID:</label>
            <input type="number" class="form-control is-valid" value="123456" disabled="disable" id="id" name="id" required>
            
            <label for="validationCustom01" class="form-label">Digite el dinero recibido:</label>
            <input type="number" class="form-control is-valid" value="${caja.dineroRecibido}" id="dineroRecibido" name="dineroRecibido" required>
            
            <label for="validationCustom01" class="form-label">Digite el cambio:</label>
            <input type="number" class="form-control is-valid" value="${caja.vueltos}" id="vueltos" name="vueltos" required>
            
            <label for="validationCustom01" class="form-label">Digite las cantidad de ventas:</label>
            <input type="number" class="form-control is-valid" value="${caja.contadorVentas}" id="contadorVentas" name="contadorVentas" required>
            
            <label for="validationCustom01" class="form-label">Digite el dinero acumulado:</label>
            <input type="number" class="form-control is-valid" value="${caja.dineroAcumulado}" id="dineroAcumulado" name="dineroAcumulado" required>
            
            
            <button class="btn btn-primary" type="submit">Guardar</button>
        </form>
<%@include file="../admin/pie.jsp" %>


<%@include file="../admin/cabecera.jsp" %>
<a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/cliente/agregarCliente.jsp">Agregar cliente</a>
<table class="table table-dark table-sm">
    <thead>
        <tr>
            <th scope="col">Cedula</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>

        </tr>
    </thead>
    <tbody>           
        <c:forEach var="cli" items="${clientes}">
            <tr>
                <td>${cli.cedula}</td>
                <td>${cli.nombre}</td>
                <td>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/cliente?cedula=${cli.cedula}&accion=borrar">Eliminar</a>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/cliente?cedula=${cli.cedula}&accion=editar">Editar</a>
                </td>
            </tr>                  
        </c:forEach>
    </tbody>
</table>
<%@include file="../admin/pie.jsp" %>
<%-- 
    Document   : verProductos
    Created on : 12/12/2021, 10:45:30 a. m.
    Author     : ANDERSON
--%>

<%@include file="../admin/cabecera.jsp" %>
<a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/producto/agregarProducto.jsp">Agregar producto</a>
<table class="table table-dark table-sm">
    <thead>
        <tr>  
            <th scope="col">ID</th>
            <th scope="col">Precio</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="pro" items="${productos}">
            <tr>
                <td>${pro.id_producto}</td>
                <td>${pro.precio}</td>
                <td>${pro.nombre}</td>
                <td>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/producto?id_producto=${pro.id_producto}&accion=borrar">Eliminar</a>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/producto?id_producto=${pro.id_producto}&accion=editar">Actualizar</a>

                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<%@include file="../admin/pie.jsp" %>
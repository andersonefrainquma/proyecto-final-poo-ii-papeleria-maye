/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.CajaRegistradoraDAO;
import Modelo.Entity.CajaRegistradora;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ANDERSON
 */
@WebServlet("/caja")
public class CajaController extends HttpServlet {

    CajaRegistradoraDAO cajDAO = new CajaRegistradoraDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "editar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    listarCaja(req, resp);
            }
        } else {
            listarCaja(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarCaja(req, resp);
            }
        } else {
            this.listarCaja(req, resp);
        }

    }

    private void listarCaja(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<CajaRegistradora> cajas = this.cajDAO.consultar();
        req.setAttribute("cajas", cajas);
        req.getRequestDispatcher("caja/verCaja.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int id;
        long dineroR;
        long vueltos;
        int contadorVentas;
        long dineroA;

        switch (n) {

            case 3: { //Editar                
                CajaRegistradora caja = this.cajDAO.consultar().get(0);

                req.setAttribute("caja", caja);
                req.getRequestDispatcher("caja/editarCaja.jsp").forward(req, resp);
                this.listarCaja(req, resp);
                break;
            }

            case 4: { //Modificar
                dineroR = Long.parseLong(req.getParameter("dineroRecibido"));
                vueltos = Long.parseLong(req.getParameter("vueltos"));
                contadorVentas = Integer.valueOf(req.getParameter("contadorVentas"));
                dineroA = Long.parseLong(req.getParameter("dineroAcumulado"));

                int registros = this.cajDAO.actualizar(new CajaRegistradora(dineroR, vueltos, contadorVentas, dineroA));
                this.listarCaja(req, resp);
                break;
            }
        }
    }

}

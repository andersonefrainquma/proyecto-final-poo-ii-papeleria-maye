/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.FacturaDAO;
import Modelo.Entity.Factura;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ANDERSON
 */
@WebServlet("/factura")
public class FacturaController extends HttpServlet {

    FacturaDAO facDAO = new FacturaDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.metodos(req, resp, 2);
                    break;
                case "editar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    listarFactura(req, resp);

            }
        } else {
            listarFactura(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.metodos(req, resp, 1);
                    break;
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarFactura(req, resp);
            }
        } else {
            this.listarFactura(req, resp);
        }

    }

    private void listarFactura(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Factura> facturas = this.facDAO.consultar();
        req.setAttribute("facturas", facturas);
        req.getRequestDispatcher("factura/verFacturas.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int id;
        long precio;

        switch (n) {
            case 1: {  //Crear
                id = Integer.valueOf(req.getParameter("id_factura"));
                precio = Long.parseLong(req.getParameter("precioTotal"));

                int registros = this.facDAO.insertar(new Factura(id, precio));
                this.listarFactura(req, resp);
                break;
            }

            case 2: {  //Borrar
                id = Integer.valueOf(req.getParameter("id_factura"));

                int registros = this.facDAO.borrar(id);
                this.listarFactura(req, resp);

                break;
            }

            case 3: { //Editar
                id = Integer.valueOf(req.getParameter("id_factura"));

                Factura fac = this.consultarFac(id);

                req.setAttribute("factura", fac);
                req.getRequestDispatcher("factura/editarFactura.jsp").forward(req, resp);
                this.listarFactura(req, resp);
                break;
            }

            case 4: { //Modificar
                id = Integer.valueOf(req.getParameter("id_factura"));
                precio = Long.parseLong(req.getParameter("precioTotal"));

                int registros = this.facDAO.actualizar(new Factura(id, precio));
                this.listarFactura(req, resp);
                break;
            }
        }
    }

    private Factura consultarFac(int id) {
        for (Factura f : this.facDAO.consultar()) {
            if (f.getId_factura() == id) {
                Factura fa = new Factura(f.getId_factura(), f.getPrecioTotal());
                return fa;
            }
        }
        return null;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.AlmacenDAO;
import Modelo.Entity.Almacen;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ANDERSON
 */
@WebServlet("/almacen")
public class AlmacenController extends HttpServlet {

    AlmacenDAO alDAO = new AlmacenDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "editar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    listarAlmacen(req, resp);
            }
        } else {
            listarAlmacen(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarAlmacen(req, resp);
            }
        } else {
            this.listarAlmacen(req, resp);
        }

    }

    private void listarAlmacen(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Almacen> almacenes = this.alDAO.consultar();
        req.setAttribute("almacenes", almacenes);
        req.getRequestDispatcher("almacen/verAlmacen.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int cantidad;
        long precioAlmacenado;

        switch (n) {

            case 3: { //Editar                
                Almacen almacen = this.alDAO.consultar().get(0);

                req.setAttribute("almacen", almacen);
                req.getRequestDispatcher("almacen/editarAlmacen.jsp").forward(req, resp);
                this.listarAlmacen(req, resp);
                break;
            }

            case 4: { //Modificar
                precioAlmacenado = Long.parseLong(req.getParameter("precioAlmacenado"));
                cantidad = Integer.valueOf(req.getParameter("cantidadP"));

                int registros = this.alDAO.actualizar(new Almacen(cantidad, precioAlmacenado));
                this.listarAlmacen(req, resp);
                break;
            }
        }
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.ClienteDAO;
import Modelo.Entity.Cliente;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ANDERSON
 */
@WebServlet("/cliente")
public class ClienteController extends HttpServlet {

    ClienteDAO cliDAO = new ClienteDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.metodos(req, resp, 2);
                    break;
                case "editar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    listarCliente(req, resp);

            }
        } else {
            listarCliente(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.metodos(req, resp, 1);
                    break;
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarCliente(req, resp);
            }
        } else {
            this.listarCliente(req, resp);
        }

    }

    private void listarCliente(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Cliente> clientes = cliDAO.consultar();
        req.setAttribute("clientes", clientes);
        req.getRequestDispatcher("cliente/verClientes.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int cedula;
        String nombre;

        switch (n) {
            case 1: {  //Crear
                cedula = Integer.valueOf(req.getParameter("cedula"));
                nombre = req.getParameter("nombre");

                int registros = cliDAO.insertar(new Cliente(cedula, nombre));
                this.listarCliente(req, resp);
                break;
            }

            case 2: {  //Borrar
                cedula = Integer.valueOf(req.getParameter("cedula"));

                int registros = cliDAO.borrar(cedula);
                this.listarCliente(req, resp);

                break;
            }

            case 3: { //Editar
                cedula = Integer.valueOf(req.getParameter("cedula"));

                Cliente cli = this.consultarCli(cedula);

                req.setAttribute("cliente", cli);
                req.getRequestDispatcher("cliente/editarCliente.jsp").forward(req, resp);
                this.listarCliente(req, resp);
                break;
            }

            case 4: { //Modificar
                cedula = Integer.valueOf(req.getParameter("cedula"));
                nombre = req.getParameter("nombre");
                int registros = this.cliDAO.actualizar(new Cliente(cedula, nombre));
                this.listarCliente(req, resp);
                break;
            }
        }
    }

    private Cliente consultarCli(int cedula) {
        for (Cliente c : cliDAO.consultar()) {
            if (c.getCedula() == cedula) {
                Cliente cli = new Cliente(c.getCedula(), c.getNombre());
                return cli;
            }
        }
        return null;
    }
}

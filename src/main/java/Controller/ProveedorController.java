/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.ProveedorDAO;
import Modelo.Entity.Proveedor;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ANDERSON
 */
@WebServlet("/proveedor")
public class ProveedorController extends HttpServlet {

    ProveedorDAO proDAO = new ProveedorDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.metodos(req, resp, 2);
                    break;
                case "editar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    listarProveedor(req, resp);

            }
        } else {
            listarProveedor(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.metodos(req, resp, 1);
                    break;
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarProveedor(req, resp);
            }
        } else {
            this.listarProveedor(req, resp);
        }

    }

    private void listarProveedor(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Proveedor> proveedores = proDAO.consultar();
        req.setAttribute("proveedores", proveedores);
        req.getRequestDispatcher("proveedor/verProveedores.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int cedula;
        String nombre;

        switch (n) {
            case 1: {  //Crear
                cedula = Integer.valueOf(req.getParameter("cedula"));
                nombre = req.getParameter("nombre");

                int registros = proDAO.insertar(new Proveedor(cedula, nombre));
                this.listarProveedor(req, resp);
                break;
            }

            case 2: {  //Borrar
                cedula = Integer.valueOf(req.getParameter("cedula"));

                int registros = proDAO.borrar(cedula);
                this.listarProveedor(req, resp);

                break;
            }

            case 3: { //Editar
                cedula = Integer.valueOf(req.getParameter("cedula"));

                Proveedor pro = this.consultarPro(cedula);

                req.setAttribute("proveedor", pro);
                req.getRequestDispatcher("proveedor/editarProveedor.jsp").forward(req, resp);
                this.listarProveedor(req, resp);
                break;
            }

            case 4: { //Modificar
                cedula = Integer.valueOf(req.getParameter("cedula"));
                nombre = req.getParameter("nombre");
                int registros = this.proDAO.actualizar(new Proveedor(cedula, nombre));
                this.listarProveedor(req, resp);
                break;
            }
        }
    }

    private Proveedor consultarPro(int cedula) {
        for (Proveedor c : proDAO.consultar()) {
            if (c.getCedula() == cedula) {
                Proveedor pro = new Proveedor(c.getCedula(), c.getNombre());
                return pro;
            }
        }
        return null;
    }
}

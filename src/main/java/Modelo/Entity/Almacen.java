/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author ANDERSON
 */
public class Almacen {
    int cantidadProductos;
    long precioAlmacenado;

    public Almacen() {
    }

    public Almacen(int cantidadProductos, long precioAlmacenado) {
        this.cantidadProductos = cantidadProductos;
        this.precioAlmacenado = precioAlmacenado;
    }

    public int getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(int cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public long getPrecioAlmacenado() {
        return precioAlmacenado;
    }

    public void setPrecioAlmacenado(long precioAlmacenado) {
        this.precioAlmacenado = precioAlmacenado;
    }

    @Override
    public String toString() {
        return "Almacen{" + "cantidadProductos=" + cantidadProductos + ", precioAlmacenado=" + precioAlmacenado + '}';
    }
    
    
}

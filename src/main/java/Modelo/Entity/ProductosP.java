/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author ANDERSON
 */
public class ProductosP {

    int id_producto;
    int precio;
    String nombre;

    public ProductosP() {
    }

    public ProductosP(int id_producto, int precio, String nombre) {
        this.id_producto = id_producto;
        this.precio = precio;
        this.nombre = nombre;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "ProductosP{" + "id_producto=" + id_producto + ", precio=" + precio + ", nombre=" + nombre + '}';
    }

}

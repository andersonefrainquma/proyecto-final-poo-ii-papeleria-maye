/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author ANDERSON
 */
public class Factura {
    
    int id_factura;
    long precioTotal;

    public Factura() {
    }

    public Factura(int id_factura, long precioTotal) {
        this.id_factura = id_factura;
        this.precioTotal = precioTotal;
    }

    public int getId_factura() {
        return id_factura;
    }

    public void setId_factura(int id_factura) {
        this.id_factura = id_factura;
    }

    public long getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(long precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public String toString() {
        return "FacturaDTO{" + "id_factura=" + id_factura + ", precioTotal=" + precioTotal + '}';
    }
    
    
}

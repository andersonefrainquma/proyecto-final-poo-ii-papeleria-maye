/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Almacen;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public class AlmacenDAO implements AlmacenService {

    public static final String SQL_CONSULTA = "SELECT * FROM almacen";
    public static final String SQL_UPDATE = "UPDATE almacen SET cantidadP = ?, precioAlmacenado = ? WHERE id = 123456";

    @Override
    public List<Almacen> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Almacen> almacen = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int cantidadP = res.getInt("cantidadP");
                long precioAlmacenado = res.getLong("precioAlmacenado");
                Almacen a = new Almacen(cantidadP, precioAlmacenado);
                almacen.add(a);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return almacen;
    }

    @Override
    public int actualizar(Almacen almacen) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, almacen.getCantidadProductos());
            ps.setLong(2, almacen.getPrecioAlmacenado());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

}

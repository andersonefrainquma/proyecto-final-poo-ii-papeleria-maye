/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Producto;
import java.util.List;

/**
 *
 * @author ANDERSON
 */
public interface ProductoService {
    public int insertar(Producto producto);

    public List<Producto> consultar();

    public int borrar(int id_producto);

    public int actualizar(Producto producto);
}

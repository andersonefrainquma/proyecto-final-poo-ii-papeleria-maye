/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Cliente;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public interface ClienteService {

    public int insertar(Cliente cliente);

    public List<Cliente> consultar();

    public int borrar(int id_cliente);

    public int actualizar(Cliente cliente);
}

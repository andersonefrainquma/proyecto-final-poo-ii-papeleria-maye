/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Red;

import java.sql.*;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author moises
 */
public class ConexionBD {

    public static final String JDBC_URL = "jdbc:mysql://107.20.53.98:3306/papeleriaMaye?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
    public static final String DB_USER = "papeleria";
    public static final String DB_CLAVE = "papeleria123456";

    //patron singleton
    public static DataSource getDataSource() {

        BasicDataSource bs = new BasicDataSource();
        bs.setUrl(JDBC_URL);
        bs.setUsername(DB_USER);
        bs.setPassword(DB_CLAVE);
        bs.setInitialSize(5);
        return bs;
    }

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public static void close(Connection con) throws SQLException {
        con.close();
    }

    public static void close(Statement stm) throws SQLException {
        stm.close();
    }

    public static void close(ResultSet res) throws SQLException {
        res.close();
    }

    public static void close(PreparedStatement ps) throws SQLException {
        ps.close();
    }

}
